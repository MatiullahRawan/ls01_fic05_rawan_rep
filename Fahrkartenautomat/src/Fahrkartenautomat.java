﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
      while(true) {
    	  int zuZahlenderBetrag = fahrkartenbestellungErfassen();
          int eingezahlterGesamtbetrag = fahrkartenBezahlen (zuZahlenderBetrag);
          fahrkartenAusgeben();
          rueckgeldAusgeben (zuZahlenderBetrag,eingezahlterGesamtbetrag);
          System.out.println("\n\n\n\n");
      }
      
   
    }
    /*
     * a = 1;
    while(a!=1 && a!=2 && a!=3){
    	//a ändern
    	a = tastatur.nextInt();
    }
    
    */
    
    public static int fahrkartenbestellungErfassen () 
    {
    	int zuZahlenderBetrag = 0;
    	int anzahlTickets = 0;
    	int Auswahl = 0; 
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n" + 
    			"  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + 
    			"  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
    			"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        while(Auswahl!=1 && Auswahl !=2 && Auswahl!=3){
        	Auswahl = tastatur.nextInt();
        	if(Auswahl != 1 && Auswahl != 2 && Auswahl != 3) {
        		System.out.println("falsche Eingabe\n");
        	}
        }
    	
    	switch (Auswahl){
    		case 1:
    			zuZahlenderBetrag = 290;
    			break;
    		case 2:
    			zuZahlenderBetrag = 860;
    			break;
    		case 3:
    			zuZahlenderBetrag = 2350;
    			break;
    		default:
    			System.out.println("Ungültgraufgabe");
    		
    		
    	}
          
          while(zuZahlenderBetrag <= 0)  {
        	  System.out.print("Zu zahlender Betrag (EURO): ");
              zuZahlenderBetrag = (int) (tastatur.nextDouble() * 100); 
              if(zuZahlenderBetrag <= 0) {
            	  System.out.println("Bitte geben einen gültigen Wert ein.");
              }
          }

          while(anzahlTickets < 1 || anzahlTickets > 10)
          {
        	  System.out.print("Anzahl dieser Ticketart: ");
              anzahlTickets = tastatur.nextInt();
              if(anzahlTickets < 1 || anzahlTickets > 10) {
            	  System.out.println("Bitte geben Sie ein Wert Zwischen 1 und 10 ein.");
              }
          }
          zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
          
          return zuZahlenderBetrag;
     
    }
    
     public static int fahrkartenBezahlen (int zuZahlenderBetrag) {
    	 int eingezahlterGesamtbetrag = 0;
    	 int eingeworfeneMünze;
    	 
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("Noch zu zahlen: %.2f\n", (((double)(zuZahlenderBetrag - eingezahlterGesamtbetrag))/100));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = (int) (tastatur.nextDouble() * 100);
           eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
         
         return eingezahlterGesamtbetrag;
    }
     
     public static void fahrkartenAusgeben()
     {
    	 
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            warten(250);
            
         }
     }
     
     public static void rueckgeldAusgeben (int zuZahlenderBetrag, int eingezahlterGesamtbetrag) {
    	 int  rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	  if(rückgabebetrag > 0)
          {
    		  muenzeAusgeben(rückgabebetrag);
    		  
      
          }
     }
     
     public static void warten(int millisekunden) {
    	 try
         {
			Thread.sleep(millisekunden);
         } catch (InterruptedException e)
         {
			// TODO Auto-generated catch block
			e.printStackTrace();
         }
     }
     
     public static  void muenzeAusgeben(int rückgabebetrag) {
    	 //FA-02: durch %.2f wird der double mit zwei Nachkommastellen ausgegeben
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", (((double)rückgabebetrag)/100));
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   
           //FA-03++.6: rechnen mit CENT Werten um Fehler zu vermeiden
     	   while(rückgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 200;
            }
            while(rückgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 100;
            }
            while(rückgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 50;
            }
            while(rückgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 20;
            }
            while(rückgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 10;
            }
            while(rückgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 5;
            }
     } 
    	 
     
}